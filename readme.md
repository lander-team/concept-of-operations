# Concept Of Operations

```mermaid
%%{init: {'theme':'neutral'}}%%
flowchart LR;
j -.no.-> h
subgraph Descent
a[Deorbit Burn] --> b[Trajectory Optimization]
b --> c[Hoverslam Altitude?]
c -.no.-> b
c -.yes.-> d[Controlled Landing]
d-->dd[Touchdown]
end

subgraph Ascent
f[Engine Ignition]
f --> g[Controlled Takoff]
g --> h[Orbital Burn] 
h --> j[Desired Orbit?]
j -.yes.->jj[Mission End]
end

dd --> g1[Unload/Load Mining Equipment]
g1 --> Ascent


```

# Avionics Design 

```mermaid

%%{init: {'theme':'neutral'}}%%
flowchart LR;

batt[Battery]
t[Teensy 4.1]
imu[Inertial Measurement Unit]
b[Barometer]

subgraph Propulsion
r[Roll Motor]
p[Pitch Motor]
e[Engine Ignitor]
end


imu & b -. I2C .-> t
batt == 3.3V ==> t

t -- PWM --> r & p & e

```

